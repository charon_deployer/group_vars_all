# Repository for storing group_vars/all.yml files

We store all.yml one for zone in separated from product inventory repository. It is mainly made to single responsibility pattern. If you change database connection parameters or other infrastructure zone produts parameters there is only one place to change it. 

Second is segregation of duties. One team usually supports several products and errors in their inventory repositories affect only their products. Error in zone wide parameters affect all products on zone. Elevated permitons are required to edit this repository. 

Jenkins copy all.yml to product inventory group_vars folder. 